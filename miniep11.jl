# MAC0110 - MiniEP11
# Nome: Miqueias Lima
# NUSP: 11796340

using Test

function palindromo(s)
    s = lowercase(s)
    n = []
    j = 1
    while j <= lastindex(s)
        push!(n, s[j])
        j = nextind(s, j)
    end
        
    i = 1
    while i <= length(n)
        if n[i] in ['á', 'à', 'â', 'ã']
            n[i] = 'a'
        elseif n[i] in ['é', 'ê', 'è', 'ẽ']
            n[i] = 'e'
        elseif n[i] == 'í'
            n[i] = 'i'
        elseif n[i] in ['ó', 'õ', 'ô']
            n[i] = 'o'
        elseif n[i] in ['ú', 'ù', 'û', 'ũ']
            n[i] = 'u'
        elseif n[i] == 'ç'
            n[i] = 'c'
        elseif n[i] in ['_', ' ', ',', '?', '!', '-', '(', ')', '/', '°', 'ª', 'º', ';', ':', '.']
            n[i] = ""
        end
        i += 1
    end
    
    h = ""
    for l in 1:length(n)
        h = h*n[l]
    end
    
    r = firstindex(h)
    o = lastindex(h)
    

    
    while r <= o
        if h[r] != h[o]
            return false
        end
        r += 1
        o -= 1
    end
    
    return true
end

function testa()
    @test palindromo("Socorram-me, subi no ^onibus em Marrocos!")
    @test palindromo("ovo")
    @test !palindromo("a mae esta aqui")
    @test palindromo("A Mãe te ama !!!?")
    @test !palindromo("testando a função")
end

testa()

